Version 4
SHEET 1 3964 680
WIRE 3664 -96 3520 -96
WIRE -304 -64 -336 -64
WIRE -192 -64 -224 -64
WIRE -128 -64 -192 -64
WIRE -16 -64 -48 -64
WIRE 48 -64 -16 -64
WIRE 160 -64 128 -64
WIRE 224 -64 160 -64
WIRE 336 -64 304 -64
WIRE 480 -64 336 -64
WIRE 768 -64 704 -64
WIRE 880 -64 848 -64
WIRE 944 -64 880 -64
WIRE 1056 -64 1024 -64
WIRE 1120 -64 1056 -64
WIRE 1232 -64 1200 -64
WIRE 1376 -64 1232 -64
WIRE 1760 -64 1680 -64
WIRE 1904 -64 1840 -64
WIRE 1968 -64 1904 -64
WIRE 2080 -64 2048 -64
WIRE 2224 -64 2080 -64
WIRE 2672 -64 2608 -64
WIRE 2784 -64 2752 -64
WIRE 2928 -64 2784 -64
WIRE 3520 -64 3520 -96
WIRE -336 -48 -336 -64
WIRE -192 -32 -192 -64
WIRE -16 -32 -16 -64
WIRE 160 -32 160 -64
WIRE 336 -32 336 -64
WIRE 704 -32 704 -64
WIRE 880 -32 880 -64
WIRE 1056 -32 1056 -64
WIRE 1232 -32 1232 -64
WIRE 1680 -32 1680 -64
WIRE 1904 -32 1904 -64
WIRE 2080 -32 2080 -64
WIRE 2608 -32 2608 -64
WIRE 2784 -32 2784 -64
WIRE 3520 48 3520 16
WIRE -192 80 -192 48
WIRE -16 80 -16 48
WIRE 160 80 160 48
WIRE 336 80 336 48
WIRE 704 80 704 48
WIRE 880 80 880 48
WIRE 1056 80 1056 48
WIRE 1232 80 1232 48
WIRE 1680 80 1680 48
WIRE 1904 80 1904 48
WIRE 2080 80 2080 48
WIRE 2608 80 2608 48
WIRE 2784 80 2784 48
WIRE 3520 144 3520 128
WIRE 3664 144 3520 144
WIRE 3520 160 3520 144
WIRE -192 176 -192 160
WIRE -16 176 -16 160
WIRE -16 176 -192 176
WIRE 160 176 160 160
WIRE 160 176 -16 176
WIRE 336 176 336 160
WIRE 336 176 160 176
WIRE 480 176 336 176
WIRE 704 176 704 160
WIRE 880 176 880 160
WIRE 880 176 704 176
WIRE 1056 176 1056 160
WIRE 1056 176 880 176
WIRE 1232 176 1232 160
WIRE 1232 176 1056 176
WIRE 1376 176 1232 176
WIRE 1680 176 1680 160
WIRE 1904 176 1904 160
WIRE 1904 176 1680 176
WIRE 2080 176 2080 160
WIRE 2080 176 1904 176
WIRE 2224 176 2080 176
WIRE 2608 176 2608 160
WIRE 2784 176 2784 160
WIRE 2784 176 2608 176
WIRE 2928 176 2784 176
WIRE -336 192 -336 32
WIRE -192 192 -192 176
WIRE -16 192 -16 176
WIRE 160 192 160 176
WIRE 336 192 336 176
WIRE 704 192 704 176
WIRE 880 192 880 176
WIRE 1056 192 1056 176
WIRE 1232 192 1232 176
WIRE 1680 192 1680 176
WIRE 1904 192 1904 176
WIRE 2080 192 2080 176
WIRE 2608 192 2608 176
WIRE 2784 192 2784 176
FLAG -336 192 0
FLAG -192 192 0
FLAG -16 192 0
FLAG 160 192 0
FLAG 336 192 0
FLAG 704 192 0
FLAG 880 192 0
FLAG 1056 192 0
FLAG 1232 192 0
FLAG 1680 192 0
FLAG 1904 192 0
FLAG 2080 192 0
FLAG 2608 192 0
FLAG 2784 192 0
FLAG 3520 160 0
SYMBOL voltage -192 64 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit0
SYMBOL res -208 -80 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMBOL res -320 48 R180
WINDOW 0 36 76 Invisible 2
WINDOW 3 36 40 Left 2
SYMATTR InstName R2
SYMBOL res -208 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R3
SYMATTR Value 2R
SYMBOL voltage -16 64 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit1
SYMBOL res -32 -80 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R4
SYMBOL res -32 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R5
SYMATTR Value 2R
SYMBOL voltage 160 64 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit2
SYMBOL res 144 -80 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R6
SYMBOL res 144 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R7
SYMATTR Value 2R
SYMBOL voltage 336 64 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit3
SYMBOL res 320 -80 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R8
SYMBOL res 320 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R9
SYMATTR Value 2R
SYMBOL voltage 704 64 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit0/2
SYMBOL voltage 880 64 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit1
SYMBOL res 864 -80 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R13
SYMBOL res 864 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R14
SYMATTR Value 2R
SYMBOL voltage 1056 64 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit2
SYMBOL res 1040 -80 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R15
SYMBOL res 1040 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R16
SYMATTR Value 2R
SYMBOL voltage 1232 64 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit3
SYMBOL res 1216 -80 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R17
SYMBOL res 1216 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R18
SYMATTR Value 2R
SYMBOL res 688 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R10
SYMBOL voltage 1680 64 R0
WINDOW 3 24 96 Invisible 2
WINDOW 0 -198 102 Left 2
SYMATTR InstName Bit0/4+Bit1/2
SYMBOL voltage 1904 64 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit2
SYMBOL res 1856 -80 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R19
SYMBOL res 1888 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R20
SYMATTR Value 2R
SYMBOL voltage 2080 64 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit3
SYMBOL res 2064 -80 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R21
SYMBOL res 2064 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R22
SYMATTR Value 2R
SYMBOL res 1664 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R11
SYMBOL voltage 2608 64 R0
WINDOW 3 24 96 Invisible 2
WINDOW 0 -271 97 Left 2
SYMATTR InstName Bit0/8+Bit1/4+Bit2/2
SYMBOL res 2592 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R23
SYMBOL voltage 2784 64 R0
WINDOW 3 24 96 Invisible 2
SYMATTR InstName Bit3
SYMBOL res 2768 -80 R90
WINDOW 0 0 56 Invisible 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R24
SYMBOL res 2768 -48 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R25
SYMATTR Value 2R
SYMBOL voltage 3520 32 R0
WINDOW 3 24 96 Invisible 2
WINDOW 0 -388 93 Left 2
SYMATTR InstName Bit0/16+Bit1/8+Bit2/4+Bit3/2
SYMBOL res 3504 -80 R0
WINDOW 0 36 40 Invisible 2
SYMATTR InstName R27
TEXT 448 48 Left 2 ;V_OUT
TEXT -200 -80 Left 2 ;A
TEXT -64 248 Left 2 ;The Circuit for 4 Bits\n \nBit0 is the LSB. Bit1 is the MSB.
TEXT -24 -80 Left 2 ;B
TEXT 152 -80 Left 2 ;C
TEXT 328 -80 Left 2 ;D
TEXT 1344 48 Left 2 ;V_OUT
TEXT 696 -80 Left 2 ;A
TEXT 800 240 Left 2 ;Thevenin Equivalent Circuit Till Point A
TEXT 872 -80 Left 2 ;B
TEXT 1048 -80 Left 2 ;C
TEXT 1224 -80 Left 2 ;D
TEXT 2192 48 Left 2 ;V_OUT
TEXT 1736 248 Left 2 ;Thevenin Equivalent Circuit Till Point B
TEXT 1664 -80 Left 2 ;B
TEXT 1896 -80 Left 2 ;C
TEXT 2072 -80 Left 2 ;D
TEXT 2896 48 Left 2 ;V_OUT
TEXT 2552 240 Left 2 ;Thevenin Equivalent Circuit Till Point C
TEXT 2600 -80 Left 2 ;C
TEXT 2776 -80 Left 2 ;D
TEXT 3632 16 Left 2 ;V_OUT
TEXT 3264 216 Left 2 ;Thevenin Equivalent Circuit Till Point D
TEXT 3512 -112 Left 2 ;D
LINE Normal 480 32 480 -64 2
LINE Normal 480 64 480 176 2
LINE Normal 1376 32 1376 -64 2
LINE Normal 1376 64 1376 176 2
LINE Normal 2224 32 2224 -64 2
LINE Normal 2224 64 2224 176 2
LINE Normal 2928 32 2928 -64 2
LINE Normal 2928 64 2928 176 2
LINE Normal 3664 0 3664 -96 2
LINE Normal 3664 32 3664 144 2
