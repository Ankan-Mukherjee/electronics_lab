#define BIT_0 2
#define BIT_1 4
#define BIT_2 7
#define BIT_3 8
#define INPUT_PIN A0

int num;
byte b0,b1,b2,b3;
float V_in;

void setBits(int n)
{
  b0=n&1;
  n>>=1;
  b1=n&1;
  n>>=1;
  b2=n&1;
  n>>=1;
  b3=n&1;
  n>>=1;
}


void setup() 
{
  num=9;
  V_in=0;
  Serial.begin(9600);
  pinMode(BIT_0, OUTPUT);
  pinMode(BIT_1, OUTPUT);
  pinMode(BIT_2, OUTPUT);
  pinMode(BIT_3, OUTPUT);
  pinMode(INPUT_PIN, INPUT);
  setBits(num);
}


void loop() 
{
  digitalWrite(BIT_0, b0==1?HIGH:LOW);  
  digitalWrite(BIT_1, b1==1?HIGH:LOW);  
  digitalWrite(BIT_2, b2==1?HIGH:LOW);  
  digitalWrite(BIT_3, b3==1?HIGH:LOW);  

  V_in=analogRead(INPUT_PIN)/1023.0*5.0;       
  Serial.println(V_in); 
}
